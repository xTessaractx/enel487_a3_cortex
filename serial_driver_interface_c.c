/**
		Name: Tessa Herzberger
		SID: 200342876
		
		Institution: University of Regina
		Class: ENEL 487
		Professor: Karim Naqvi

		Assignment: #3
		File Name: serial_driver_interface.c
		Due: 3 December 2017
*/

#include <stdint.h>
#include "serial_driver_interface.h"
#include "main.h"

/**
	The serial_open function initializes the registers needed to
	enable the serial port on the Cortex-M3 lab board.
	It takes nothing and returns nothing.
*/
void serial_open(void)
{
	*regRCC_APB2ENR |= 0x0000000D; //Enable GPIOA Clock, GPIOB Clock and AFIO Clocks.
	
	*regGPIOA_CRL	|= 	0x00000B80;	//Enable Tx (Alt. Output push/pull, 50MHz; 1011) and Rx (floating input; 1000)of USART2
	
	*regRCC_APB1ENR |= 0x00020000; //Enable USART2 Clock
	*regRCC_APB1ENR |= 0x00040000;

	*regUSART_CR1 |=  0x0000200C; //Enable USART Transmission and Reception.
	
	//Configure Baud Rate
	*regUSART_BRR |= 36000000/9600;
	*regUSART_CR1 &= 0xFFFFEBFF; //set m bit and pce to zero
	*regUSART_CR2 &= 0xFFFFCFFF; //set stop bit to zero
}

/**
	The sendbyte function takes a char,
	then prints it to the console.
	It returns nothing.
*/
void sendbyte(char data)
{
	uint32_t USART_SR_TXE = 0x00000080;
	//Check if TXE is high
	while ((*regUSART_SR & USART_SR_TXE) == 0)
	{
	}
	
	* regUSART_DR = data;

	return;
}

/**
	The getbyte function obtains a character
	from the serial port, then returns it.
	It takes no parameters and returns a char.
*/
char getbyte(void)
{		
	uint32_t USART_SR_RNXE = 0x00000020;
	//Check if RNXE is high
	while ((*regUSART_SR & USART_SR_RNXE) == 0)
	{
	}
	
	return * regUSART_DR;
}
