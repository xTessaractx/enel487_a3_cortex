/**
Filename: serial_driver_ifacenterface.h
Author: Tessa Herzberger and Kiley LeClair
Class: enel487
Project: lab3
*/

#ifndef SERIAL_DRIVER_IFACE_H
#define SERIAL_DRIVER_IFACE_H

void serial_open(void);
void sendbyte(char data);
char getbyte(void);
#endif
