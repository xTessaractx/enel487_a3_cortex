/**
		Name: Tessa Herzberger
		SID: 200342876
		
		Institution: University of Regina
		Class: ENEL 487
		Professor: Karim Naqvi

		Assignment: #3
		File Name: main.h
		Due: 3 December 2017
*/

/**This file contains all of the register pointers,
static constant integers, and function prototypes used within the main.c file. */


#ifndef MAIN_H
#define MAIN_H

#include "registers.h"
#include <stdint.h>

//Registers are defined in register.h and register.c
extern volatile uint32_t * regRCC_APB2ENR;	 

//Serial
extern volatile uint32_t * regGPIOA_CRL; //DONE
extern volatile uint32_t * regGPIOA_ODR; //DONE
extern volatile uint32_t * regGPIOA_CRH; //DONE
extern volatile uint32_t * regGPIOA_BSRR; //DONE
extern volatile uint32_t * regGPIOA_BRR; //DONE

//USART Stuff
extern volatile uint32_t * regRCC_APB1ENR;
extern volatile uint32_t * regUSART_CR1;
extern volatile uint32_t * regUSART_CR2;
extern volatile uint32_t * regUSART_BRR;
extern volatile uint32_t * regUSART_DR;
extern volatile uint32_t * regUSART_SR;

void print_string(char [100]);
void restart(char data_array[50]);

#endif

