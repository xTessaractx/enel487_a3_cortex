/**
		Name: Tessa Herzberger
		SID: 200342876
		
		Institution: University of Regina
		Class: ENEL 487
		Professor: Karim Naqvi

		Assignment: #3
		File Name: main.c
		Due: 3 December 2017
*/

#include "main.h"
#include <stdio.h>
#include "serial_driver_interface.h"
#include <time.h>
#include <stdlib.h>
#include "dictionary.h"

/**
This is the main function. It initializes the values of many variables, then determines what input the user selected.
Finally, it resets everything.
*/
int main()
{
	//Initialize the loop counter, done, i and the data_array
	int j = 0;
	int done = 0;
	char user_input[50];
	int rand_1 = 0;
	int rand_2 = 0;
	int length = 0;
	int i = 0;
	int w = 0;
	char password[25] = {'\0','\0','\0','\0','\0','\0','\0','\0','\0','\0','\0','\0','\0',
												'\0','\0','\0','\0','\0','\0','\0','\0','\0','\0','\0','\0',};

	//Call the setupRegs function and serial_open function.
	setupRegs();
	serial_open();
	
	restart(user_input);
	
	while(1)
	{
		i = 0;
		//Initialize the data_array values to Null.
		for (i = 0; i < 50; i++)
			user_input[i] = '\0';
		
		//Print the console.
		print_string("\n\r");
		print_string("Please enter 1 to generate a password.");
		print_string("\n\r");
		print_string("Please enter 0 to quit.");
		print_string("\n\r");
		
		//Ensure that I is set to zero.
		j = 0;
		done = 0;
		
		while (done != 1)
		{
				//Get the user input.
				user_input[j] = getbyte();
				length++;
				
				//Clear the value in the GPIOA_CRL register	
				* regGPIOA_CRL = 0x44444B44;
				
				sendbyte(user_input[j]);
				
				if (user_input[j] == 0xD)
				{
					sendbyte(0xD);
					done = 1;
				}
							j++;
			}
			
			//Determine if the user has entered a valid value.
			if (user_input[0] != '0' && user_input[0] != '1')
			{
				print_string("\n\r");
				print_string("You have entered an invalid value.");
				print_string("\n\r");
			}
			
			else if (user_input[0] == '1' && user_input[1] == 0xD)
			{
				i = 0;
				
						for (i = 0; i < 5; i++)
						{
							for (w = 0; w < 25; w++)
								password[w] = '\0';
							
							//Re-initialize length to 0.
							length = 0;

							//Generate the two random numbers
							rand_1 = rand();
							rand_2 = rand();

							//Make sure rand_1 is positive
							if (rand_1 < 0)
								rand_1 = rand_1 * -1;

							//Make sure rand_2 is positive
							if (rand_2 < 0)
								rand_2 = rand_2 * -1;

							//Concatinate the two random numbers together.
							while (rand_2 > 10)
							{
								rand_1 *= 10;
								rand_1 += (rand_2 % 10);
								rand_2 /= 10;
							}

							//Make sure rand_1 is positive
							if (rand_1 < 0)
								rand_1 = rand_1 * -1;

							//Ensure the random number is within the length of the password_dictionary array
							rand_1 %= 642705; //642705

							if (rand_1 >= 642695) //642695
								rand_1 -= 20;

							//Search for the start of the next word
							while(password_dictionary[rand_1] != '\n')
								rand_1++;

							//Make sure rand_1 is positive
							if (rand_1 < 0)
								rand_1 = rand_1 * -1;

							//Copy the word from password_dictionary into the password array
							while(password_dictionary[rand_1+1] != '\n')
							{
								password[length] = password_dictionary[rand_1+1];
								length++;
								rand_1++;
							}

							w = 0;
							//Print the password
							while (password[w] != '\0')
							{
								sendbyte(password[w]);
								w++;
							}
							sendbyte(' ');
						}				
			}
			else if (user_input[0] == '0' && user_input[1] == 0xD)
				break;
		
		//Call the restart finction to do it all again! :)
		restart(user_input);
	}
}		

/**
The print_string function takes a const char array,
then prints the array. It returns nothing.
*/
void print_string(char output[])
{
	int i = 0;
	
	while (output[i] != '\0')
	{
		sendbyte(output[i]);
		i++;
	}
	
	return;
}

/**
The Restart function takes a character array and resets
all of its values to a null character.
It also resets the GPIOA_CRL register.
It returns nothing.
*/
void restart(char data_array[50])
{
	//Declare a loop counting variable.
	int i = 0;
	
	//Reset the data_array.
	for (i = 0; i < 50; i++)
		data_array[i] = '\0';
	
	//Reset the GPIOA_CRL register
		* regGPIOA_CRL = 0x44444B44;
	
	return;
}
