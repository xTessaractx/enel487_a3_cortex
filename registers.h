/**
		Name: Tessa Herzberger
		SID: 200342876
		
		Institution: University of Regina
		Class: ENEL 487
		Professor: Karim Naqvi

		Assignment: #3
		File Name: registers.h
		Due: 3 December 2017
*/

/**
This file contains all of the addresses for the registers, as well as the setupRegs function prototype.
*/

#ifndef REGISTERS_H
#define REGISTERS_H

#define PERIPH_BASE           ((uint32_t)0x40000000)
#define AHBPERIPH_BASE        (PERIPH_BASE + 0x20000)
#define RCC_BASE              (AHBPERIPH_BASE + 0x1000)
#define RCC_APB2ENR           (RCC_BASE + 0x18)
#define APB2PERIPH_BASE       (PERIPH_BASE + 0x10000)
#define GPIOB_BASE            (APB2PERIPH_BASE + 0x0C00)

//Serial Registers
#define GPIOA_BASE       ((uint32_t)0x40010800)
#define GPIOA_CRL     (GPIOA_BASE)
#define GPIOA_ODR        (GPIOA_BASE + 0x0C)
#define GPIOA_CRH   (GPIOA_BASE + 0x04)
#define GPIOA_BSRR   	   (GPIOA_BASE  + 0x10)
#define GPIOA_BRR       (GPIOA_BASE  + 0x14)

//USART Registers
#define RCC_APB1ENR		 (RCC_BASE + 0x1C)
#define USART_BASE    (0x40004400)
#define USART_CR1     (USART_BASE +0x0C) //USART Enable
#define USART_CR2     	   (USART_BASE +0x10) //USART Clock Enable
#define USART_BRR       (USART_BASE  + 0x08)
#define USART_DR      (USART_BASE + 0x04)
#define USART_SR     	   (USART_BASE)

void setupRegs(void);

#endif
