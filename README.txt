Program: ENEL487_A2.cpp
Name: Tessa Herzberger
SID: 200342876
Institution: University of Regina
Class: ENEL 487
Professor: Karim Naqvi
Assignment: #3
Due: 3 December 2017


GENERAL USAGE NOTES:
- This assignment was broken into two separate programs, each with their own repository:
	- One that works on snoopy (repository name: ENEL487_A3_Snoopy)
	- One that works on the Cortex-M3 lab board (repository name: ENEL487_A3_Cortex)
- There is only major differences between the two programs:
	- Method of outputting and inputtiing text
		- The program that works on snoopy simply uses cin to input text and cerr to output text to the console.
		- The program that works on the Cortex-M3 board uses a serial connection and USART to input and output text to the console.
- Both repositories	include the following identical files, as these files are used in both programs:
	- README.txt
	- preprocessed.py
	- dictionary.txt
	- preprocessed.txt
		
CONSTRUCTING THE DICTIONARY
- In order to construct the dictionary, three GREP commands were ran on the original american-english file (provided by the professor) in snoopy.
	- The below line was ran to remove all words with apostraphies.
	 	grep -v ".*.*$" american-english > american-english2
	- The below line was ran to remove all words with less than five characters.
	 	grep -E '^.{5}$' american-english2 > american-english3
	- The below line was ran to remove all words with unicode characters.
	 	grep -P -v '[^\x00-\x7f]' american-english3 > preprocessed
- After the appropriate file was obtained (preprocessed.txt), and a python script called preprocessed.py was created.
	- The script converted the dictionary file into a character array format that is usable by the C and C++ programs.
		
THE PROGRAM THAT WORKS ON SNOOPY:
 - This program consists of one function, the main.
	- The main function asks for the user to enter a 0 if they want to quit or a 1 to generate a password.
		This process repeats until 0 is entered to exit the do-while loop and the program.
		- The following is run five times (each time the user enters a 1) in order to generate the five word password.
			- The function generates two random numbers which are concatinated together to create one random number.
			- The function searches for the start of the next word (and makes sure that the end of the file is not reached).
			- The function then copies the word from the password_dictionary array into a local password array, then the password array is printed to the screen.
 - Compilint and Running:
	- In order to compile and run this program in command line, use the following lines:
		g++ -o A1 ENEL487_A3.cpp
		./A1
- Memory Usage and Size:
	- Text: 3575
	- Data: 643385
	- BSS: 568
	- Dec: 647528
	- Hex: 9E168

THE PROGRAM THAT WORKS ON THE CORTEX-M3 LAB BOARD:
 - This program consists of six functions:
 	- The main function:
		- The main function asks for the user to enter a 0 if they want to quit or a 1 to generate a password.
			This process repeats until 0 is entered to exit the do-while loop and the program.
		- The following is run five times (each time the user enters a 1) in order to generate the five word password.
			- The function generates two random numbers which are concatinated together to create one random number.
			- The function searches for the start of the next word (and makes sure that the end of the file is not reached).
			- The function then copies the word from the password_dictionary array into a local password array, then the password array is printed to the screen.
	- The restart function:
		- The Restart function takes a character array and resets all of its values to a null character.
		- It also resets the GPIOA_CRL register.
		- It returns nothing.
	- The print_string function:
		- The print_string function takes a const char array, then prints the array.
		- It returns nothing.
	- The serial_open function:
		- The serial_open function initializes the registers needed to enable the serial port on the Cortex-M3 lab board.
		- It takes nothing and returns nothing.
	- The sendbyte function:
		- The sendbyte function takes a char, then prints it to the console.
		- It returns nothing.
	- The getbyte function:
		The getbyte function obtains a character from the serial port, then returns it.
		It takes no parameters and returns a char.
 - Compilint and Running:
	- In order to compile and run this program, use Keil Tools.
- Memory Usage and Size:
	- Code: 1856
	- Ro-Data: 643044
	- Rw-Data: 48
	- Zi-Data: 1864
